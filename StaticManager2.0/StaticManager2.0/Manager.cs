﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticManager2._0
{
    static class Manager
    {
        public static List<Group> ShuffleAndGetGroups(string groupName, List<Teacher> teachers, List<Student> students)
        {
            Shuffle(teachers);
            Shuffle(students);
            return GetGroups(groupName,teachers,students);
        }
        public static List<Group> GetGroups(string groupName, List<Teacher> teachers, List<Student> students)
        {
            int count = teachers.Count;
            var groups = new List<Group>(teachers.Count);
            foreach (var teacher in teachers)
            {
                var group = new Group { Name = groupName, Students = MoveStudents(students, students.Count / count), Teacher = teacher };
                count--;
                groups.Add(group);
            }
            return groups;
        }
        private static void Shuffle(List<Student> source)
        {
            Random rnd = new Random();
            for (int i = 0; i < source.Count; i++)
            {
                int num = rnd.Next(0, source.Count);
                var temp = source[i];
                source[i] = source[num];
                source[num] = temp;
            }
        }
        private static void Shuffle(List<Teacher> source)
        {
            Random rnd = new Random();
            for (int i = 0; i < source.Count; i++)
            {
                int num = rnd.Next(0, source.Count);
                var temp = source[i];
                source[i] = source[num];
                source[num] = temp;
            }
        }
        private static List<Student> MoveStudents(List<Student> students, int count)
        {
            
            List<Student> shuffledStudents = new List<Student>();
            for (int i = 0; i < count; i++)
            {
                shuffledStudents.Add(students[0]);
                students.RemoveAt(0);
            }
            return shuffledStudents;
        }
    }
}
